import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {scaleTime, scaleLinear} from 'd3-scale';
import {timeDay} from 'd3-time';
import _merge from 'lodash-es/merge';
import _round from 'lodash-es/round';
import _max from 'lodash-es/max';
import _floor from 'lodash-es/floor';
import _sum from 'lodash-es/sum';
import ToolTip from 'rc-tooltip';

import Cursor from './Cursor';
import './tipDark.css';

const marginLeft = 50;
const marginRight = 45;
const marginTop = 5;
const marginBottom = 5;
const axisHeight = 20;
const spaceBetweenTicks = 70;
const barsBetweenTicks = 8;

const Colors = {
    light: '#ececec',
    medium: '#b4b4b4',
    mediumDark: '#545454',
    dark: '#333333',
    itemFillSelected: '#ff8080',
};

const styles = {
    axis: {
        axisStyle : {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        nowStyle : {
            fill: Colors.mediumDark,
            stroke: Colors.mediumDark,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        arrowStyle : {
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        limitMarkerStyle : {
            fill: 'none',
            stroke: Colors.medium,
            strokeWidth: '1px',
            strokeLinecap: 'butt',
            strokeLinejoin: 'miter',
            strokeOpacity: 1
        },
        timeAxisTextStyle : {
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '10px',
            lineHeight: '125%',
            fontFamily: 'Roboto, sans-serif',
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: 'none',
            textAlign: 'center',
            textAnchor: 'middle',
            userSelect: 'none'
        },
        verticalAxisTextStyle : {
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: '10px',
            lineHeight: '100%',
            fontFamily: 'Roboto, sans-serif',
            fill: Colors.medium,
            fillOpacity: 1,
            stroke: 'none',
            textAlign: 'end',
            textAnchor: 'end',
            userSelect: 'none'
        }
    },
    histo: {
        common:{
            bar:{
                fillOpacity:1,
                fillRule: 'evenodd',
                strokeWidth:'1',
                strokeLinecap:'butt',
                strokeLinejoin:'miter',
                strokeMiterlimit:'1',
                strokeDasharray:'none',
                strokeDashoffset:'0',
                strokeOpacity:'1'
            },
            legend: {
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontSize: '12.5px',
                lineHeight: '125%',
                fontFamily: 'sans-serif',
                letterSpacing: '0px',
                wordSpacing: '0px',
                fillOpacity:1,
                fillRule: 'evenodd',
                stroke:'none',
                textAlign:'end',
                textAnchor:'end',
                userSelect: 'none'
            }
        }
    }
};
const defaultLabels = {
    forwardButtonTips: {
        extendForward: 'Extend forward',
        slideForward: 'Slide forward',
    },
    backwardButtonTips: {
        extendBackward: 'Extend backward',
        slideBackward: 'Slide backward',
    },
    resetButtonTip: 'Reset time span',
    gotoNowButtonTip: 'Goto Now',
    doubleClickMaxZoomMsg: 'Cannot zoom anymore!',
    scrollMaxZoomMsg: 'Cannot zoom anymore!',
    zoomInWithoutChangingSelectionMsg: 'Please change time selection before clicking on zoom ;)',
    zoomSelectionResolutionExtended: 'You reached maximum zoom level',
    maxSelectionMsg: 'You reached maximum selection',
    gotoCursor: 'Goto Cursor',
    zoomInLabel: 'Zoom in',
    zoomOutLabel: 'Zoom out',
};

export default class TimeLine extends Component{
    constructor(props){
        super(props);

        this.state = {
            domain: props.domains[0],
            start: props.timeSpan.start,
            stop: props.timeSpan.stop,
            maxZoom: false,
            maxTimespan: false,
            waitForLoad: false,
            marginLeft,
            histoWidth: props.width - marginLeft - marginRight
        };

        this.labels = _merge(defaultLabels, props.labels);
        this.xAxis = null; //time axis computation function
        this.ticks = null; //marks on x axis
        this.widthOfLastUpdate = null;

        //manage auto sliding
        this.autoMove = null;
        this.isAutoMoving = false;
        this.lastAutoMovingDelta = 0;
        this.movedSinceLastFetched = 0;
    }

    componentDidMount(){
        if(this.state.domain){
            this.getItems(this.props, this.state.domain);
        }
        else{
            this.props.onLoadDefaultDomain();
        }
        this.computeLeftMargin(this.props);
    }

    componentDidUpdate(prevProps, prevState) {
        //reached max selection
        if(!prevState.maxTimespan && this.state.maxTimespan){
            this.props.onShowMessage(this.labels.maxSelectionMsg);
        }

        //Change of width (big): update item
        if(prevProps.width !== this.props.width){
            if(this.isActive){
                this.getItems(this.props, this.state.domain, this.widthOfLastUpdate && Math.abs(this.props.width - this.widthOfLastUpdate) > 30);
            }
            else{
                this.setState({
                    histoWidth: this.props.width - this.state.marginLeft - marginRight
                });
            }
        }

        //If selection changed
        if(!prevProps.timeSpan.start.isSame(this.props.timeSpan.start)
            || !prevProps.timeSpan.stop.isSame(this.props.timeSpan.stop)){

            const newDomains = this.shiftDomains(this.props.domains, {
                min: this.props.timeSpan.start,
                max: this.props.timeSpan.stop
            });

            if(newDomains !== this.props.domains){
                this.props.onUpdateDomains(newDomains);
            }

            //Update cursor
            this.setState({
                start: this.props.timeSpan.start,
                stop: this.props.timeSpan.stop
            });
        }

        //If change in current domain, update items and state
        if(prevProps.domains[0] !== this.props.domains[0]){
            this.setState({
                domain: this.props.domains[0]
            });
            this.getItems(this.props, this.props.domains[0]);
        }

        //If we got new histo change indicator
        if(prevProps.histo !== this.props.histo){
            this.setState({
                waitForLoad: false
            });
        }

        //Recompute margin on legend change
        if(prevProps.metricsDefinition.legends !== this.props.metricsDefinition.legends){
            this.computeLeftMargin(this.props);
        }
    }

    computeLeftMargin(props){
        const maxLength = _max(props.metricsDefinition.legends.map(s => s.length));
        const newMarginLeft = _max([5 + maxLength*9, marginLeft]);
        this.setState({
            marginLeft: newMarginLeft,
            histoWidth: this.props.width - newMarginLeft - marginRight
        });
    }

    getItems(props, domain, shouldReload = true){
        const histoWidth = props.width - this.state.marginLeft - marginRight;

        this.xAxis = scaleTime()
            .domain([domain.min, domain.max])
            .range([0, histoWidth]);
        this.xAxis.clamp(true);

        this.ticks = this.xAxis.ticks(_floor(histoWidth / spaceBetweenTicks));

        const intervalMs = _max([_round(moment(this.ticks[1]).diff(moment(this.ticks[0]))/barsBetweenTicks), this.props.smallestResolution.asMilliseconds()]);

        this.setState({
            histoWidth
        });

        if(shouldReload && intervalMs){
            this.widthOfLastUpdate = props.width;
            this.setState({
                waitForLoad: true
            });
            props.onLoadHisto(intervalMs, domain.min, domain.max);
        }
    }

    render() {
        this.isActive = this.xAxis !== null;

        return  (
            <svg
                id="timeLine"
                width={this.props.width}
                height={this.props.height}
                onWheel={this.isActive ? this.onWheel : undefined}
            >
                <g transform={`translate(${this.state.marginLeft},${marginTop})`}>
                    {
                        this.isActive && this.props.histo.items &&
                            this.renderHisto()
                    }
                    { this.renderAxis() }
                    { this.renderTools() }
                    { this.renderVerticalAxis() }
                    { this.renderHistoLegend() }
                    { this.isActive && this.props.tools.cursor!==false && <this.Cursor/> }
                </g>
            </svg>
        )
    }

    renderTools() {
        const axisYPos = this.props.height - marginBottom - axisHeight;
        const doubleArrow = 'm 0,0 5,5 0,-5 5,5 -5,5 0,-5 -5,5 Z';

        return (
            <g id="timeLineTools"
               transform={`translate(0, ${axisYPos})`}
            >
                {this.props.tools.slideForward!==false &&
                <this.Button
                    id="TimelineForthButton"
                    tipPlacement="left"
                    tipOverlay={this.props.domains.length === 1 ? this.labels.forwardButtonTips.extendForward : this.labels.forwardButtonTips.slideForward }
                    position={{x: this.state.histoWidth + 22, y: -5}}
                    scale={1}
                    path={doubleArrow}
                    iconSize={10}
                    onClick={() => {this.shiftTimeLine(-120)}}
                />
                }

                {this.props.tools.slideBackward!==false &&
                <this.Button
                    id="TimelineBackButton"
                    tipPlacement="right"
                    tipOverlay={this.props.domains.length === 1 ? this.labels.backwardButtonTips.extendBackward : this.labels.backwardButtonTips.slideBackward}
                    position={{x: -7, y: 5}}
                    scale={-1}
                    path={doubleArrow}
                    iconSize={10}
                    onClick={() => {
                        this.shiftTimeLine(120)
                    }}
                />
                }

                {this.props.tools.resetTimeline!==false &&
                <this.Button
                    id="ResetTimeButton"
                    tipPlacement="left"
                    tipOverlay={this.labels.resetButtonTip}
                    position={{x: this.state.histoWidth + 17, y: -30}}
                    scale={0.75}
                    path='M5 15H3v4c0 1.1.9 2 2 2h4v-2H5v-4zM5 5h4V3H5c-1.1 0-2 .9-2 2v4h2V5zm14-2h-4v2h4v4h2V5c0-1.1-.9-2-2-2zm0 16h-4v2h4c1.1 0 2-.9 2-2v-4h-2v4zM12 9c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z'
                    iconSize={24}
                    onClick={this.props.onResetTime}
                />
                }

                {this.props.tools.gotoNow!==false &&
                <this.Button
                    id="GotoNowButton"
                    tipPlacement="left"
                    tipOverlay={this.labels.gotoNowButtonTip}
                    position={{x: this.state.histoWidth + 17, y: this.props.tools.resetTimeline!==false ? -53 : -30}}
                    scale={0.75}
                    path='M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm.5-13H11v6l5.25 3.15.75-1.23-4.5-2.67z'
                    iconSize={24}
                    onClick={this.onGoto}
                />
                }
            </g>
        );
    }

    renderAxis(){
        const axisYPos = this.props.height - marginBottom - axisHeight;
        const marks = this.ticks;
        const now = moment();

        return (
            <g id="timeLineAxis"
               transform={`translate(0, ${axisYPos})`}
            >
                {/* Horizontal Axis */}
                <path style={styles.axis.axisStyle} d={`M 0,-6 0,6`}/>
                <path style={styles.axis.axisStyle} d={`M 0,0 ${this.state.histoWidth + 13},0`}/>
                <path style={styles.axis.arrowStyle} d={`m ${this.state.histoWidth + 13},-5 5,5 -5,5 Z`}/>

                {/* Marks */}
                {
                    marks && marks.map((m, index) => (
                        <g
                            key={m.toISOString()}
                            transform={`translate(${this.xAxis(m)},0)`}
                        >
                            <path style={styles.axis.limitMarkerStyle} d='M 0,0 0,6'/>
                            <text x={0}
                                  y={15}
                                  style={{
                                      ...styles.axis.timeAxisTextStyle,
                                      fill: timeDay(m) >= m ?
                                          Colors.mediumDark : Colors.medium
                                  }}
                             >
                                {this.props.onFormatTimeLegend(m)}
                            </text>
                        </g>
                        )
                    )
                }

                {/* Now */}
                { this.xAxis && now.isSameOrBefore(this.state.domain.max) && now.isSameOrAfter(this.state.domain.min) &&
                    <path transform={`translate(${this.xAxis(now)},0)`} style={styles.axis.nowStyle} d={`m -3,6 6,0 -3,-6 Z`}/>
                }
            </g>
        );
    }

    renderHisto(){
        const maxHeight = this.props.height - marginBottom - marginTop - axisHeight;
        const intervalHisto = this.xAxis(moment(this.state.domain.min).add(this.props.histo.intervalMs));
        const maxScale = _max(this.props.histo.items.map(i => i.total));

        const histoScale = scaleLinear()
            .domain([0, maxScale])
            .range([0, maxHeight]);

        return <g id="timeLineHisto" transform={`translate(0, ${this.props.height - marginBottom - axisHeight})`}>
            {/* Histogram */}
            {
                this.props.histo.items.map((item, i) => (
                    item.total > 0
                    && item.time.isSameOrAfter(this.state.domain.min)
                    && item.time.isBefore(this.state.domain.max) &&
                    <g key={i}>
                        {
                            this.props.metricsDefinition.colors.map((color,i) => (
                                item.metrics[i] > 0 &&
                                <rect
                                    key={i}
                                    style={{
                                        ...styles.histo.common.bar,
                                        fill: color.fill,
                                        stroke: color.stroke,
                                    }}
                                    x={this.xAxis(item.time)}
                                    y={-histoScale(_sum(item.metrics.slice(0, i + 1)))}
                                    width={intervalHisto}
                                    height={histoScale(item.metrics[i])}
                                />
                            ))
                        }
                    </g>
                    )
                )
            }
        </g>;
    }

    renderHistoLegend(){
        const fill = [...this.props.metricsDefinition.colors].reverse();
        const legends = [...this.props.metricsDefinition.legends].reverse();
        return <g id="timeLineHistoLegend">
            {/* Histogram Legend */}
            {legends.map((leg,i) => (
                <text
                    key={i}
                    x={-5}
                    y={marginTop+14*(i+1)+2}
                    style={{
                        ...styles.histo.common.legend,
                        fill: fill[i].text
                    }}
                >
                    <tspan>{leg}</tspan>
                </text>
            ))}
        </g>
    }

    renderVerticalAxis(){
        const axisYPos = this.props.height - marginBottom - axisHeight;
        const maxHeight = this.props.height - marginBottom - marginTop - axisHeight;
        const maxScale = this.props.histo.items ? _max(this.props.histo.items.map(i => i.total)) || 0 : 0;
        const verticalScale = scaleLinear()
            .domain([0, maxScale])
            .range([0, maxHeight]);

        const marks = [maxScale];

        return (
            <g id="timeLineVerticalAxis"
               transform={`translate(0, ${axisYPos})`}
            >
                <path style={styles.axis.axisStyle} d={`M 0,0 0, ${-maxHeight}`}/>
                <path style={styles.axis.arrowStyle} d={`m -2.5,${-maxHeight} 5,0 -2.5,-5 Z`}/>

                {/* Marks */}
                {
                    marks.map((m, index) => (
                            <g
                                key={index}
                                transform={`translate(0,${-verticalScale(m)+5})`}
                            >
                                {m > 0 &&
                                    <text x={-5}
                                          y={-5}
                                          style={styles.axis.verticalAxisTextStyle}
                                    >
                                        {this.props.onFormatMetricLegend(m)}
                                    </text>
                                }
                            </g>
                        )
                    )
                }

            </g>
        );
    }

    Cursor = () => {
        return (
            <Cursor
                startPos={this.xAxis(this.state.start)}
                startIsOutOfView={this.state.start.isBefore(this.state.domain.min) || this.state.start.isAfter(this.state.domain.max)}
                cursorIsBeforeView={this.state.start.isBefore(this.state.domain.min) && this.state.stop.isBefore(this.state.domain.min)}
                cursorIsAfterView={this.state.start.isAfter(this.state.domain.max) && this.state.stop.isAfter(this.state.domain.max)}
                endPos={this.xAxis(this.state.stop)}
                endIsOutOfView={this.state.stop.isBefore(this.state.domain.min) || this.state.stop.isAfter(this.state.domain.max)}
                height={this.props.height - marginTop - marginBottom - axisHeight}
                overlayHeight={this.props.height - marginTop}
                overlayWidth={this.state.histoWidth}
                canZoom={true}
                minZoom={this.props.domains.length === 1}
                maxZoom={this.state.maxZoom}
                startText={this.props.onFormatTimeToolTips(this.state.start)}
                stopText={this.props.onFormatTimeToolTips(this.state.stop)}
                maxSize={this.state.maxTimespan}
                gotoCursorLabel={this.labels.gotoCursor}
                zoomInLabel={this.labels.zoomInLabel}
                zoomOutLabel={this.labels.zoomOutLabel}

                zoomIn={this.zoomIn}
                zoomOut={this.zoomOut}
                onResizeLeftCursor={this.onResizeLeftCursor}
                onResizeRightCursor={this.onResizeRightCursor}
                onEndResizeCursor={this.onEndChangeCursor}
                onDragCursor={this.onDragCursor}
                onEndDragCursor={this.onEndChangeCursor}
                onStartDrawCursor={this.onStartDrawCursor}
                onDrawCursor={this.onDrawCursor}
                onEndCursor={this.onEndChangeCursor}
                onMoveDomain={this.moveTimeLine}
                onMovedDomain={this.movedTimeLine}
                onGotoCursor={this.onGotoCursor}
            />
        );
    };

    Button = ({id, tipPlacement, tipOverlay, path, position, scale, iconSize, onClick}) => (
        <ToolTip
            placement={tipPlacement}
            prefixCls={'rc-tooltip-dark'}
            overlay={tipOverlay}
            mouseEnterDelay={0.5}
            destroyTooltipOnHide={true}
        >
            <g
                id={id}
                onMouseOver={(event) => {
                    if(this.isActive){
                        this[id].style.fill = Colors.itemFillSelected;
                        this[id].style.stroke = Colors.itemFillSelected;
                    }
                }}
                onMouseOut={(event) => {
                    if(this.isActive) {
                        this[id].style.fill = styles.axis.arrowStyle.fill;
                        this[id].style.stroke = styles.axis.arrowStyle.stroke;
                    }
                }}
                onClick={() => {
                    this.isActive && onClick();
                }}
                transform={`translate(${position.x},${position.y}) scale(${scale})`}
                style = {{
                    cursor: this.isActive ? 'pointer' : 'not-allowed'
                }}
            >
                <rect
                    x ='0'
                    y ='0'
                    height = {iconSize}
                    width = {iconSize}
                    style = {{fill: 'transparent'}}
                />
                <path
                    ref={value => this[id] = value}
                    style={styles.axis.arrowStyle}
                    d={path}
                />
            </g>
        </ToolTip>
    );

    shiftDomains(domains, {min, max}){
        //update domains if selection is outside domains
        let toUpdate = false;
        const newDomains = domains.map((domain, index) => {
            if((min && min.isBefore(domain.min)) || (max && max.isAfter(domain.max))){
                toUpdate = true;
                if(index === domains.length - 1){
                    //if last domain, increase domain
                    const newDomain = {
                        min: min && min.isBefore(domain.min) ? min : domain.min,
                        max: max && max.isAfter(domain.max) ? max : domain.max
                    };

                    if(this.props.biggestVisibleDomain && moment(newDomain.min).add(this.props.biggestVisibleDomain).isBefore(newDomain.max)){
                        if(min.isBefore(domain.min)){
                            newDomain.max = moment(min).add(this.props.biggestVisibleDomain);
                        }
                        else {
                            newDomain.min = moment(max).subtract(this.props.biggestVisibleDomain);
                        }
                    }

                    return newDomain;
                }
                else{
                    //if not last domain, shift domain
                    if(min && min.isBefore(domain.min)){
                        return {
                            min,
                            max: moment(domain.max).subtract(domain.min.diff(min))
                        }
                    }
                    else{
                        return {
                            min: moment(domain.min).add(max.diff(domain.max)),
                            max
                        }
                    }

                }
            }
            else{
                return domain;
            }
        });
        return toUpdate ? newDomains : domains;
    }

    onResizeLeftCursor=(delta, mouse) => {
        let newStart = moment(this.xAxis.invert(this.xAxis(this.state.start) + delta));
        let newStop = this.state.stop;
        let maxTimespan = false;

        if (newStart.isSameOrAfter(newStop)) {
            newStop = moment(this.xAxis.invert(this.xAxis(newStop) + delta));
        }

        if(this.props.biggestTimeSpan){
            const min = moment(newStop).subtract(this.props.biggestTimeSpan);
            if(min.isSameOrAfter(newStart)){
                newStart = min;
                maxTimespan = true;
            }
        }

        this.handleAutoSliding(mouse, this.onResizeLeftCursor);

        if ((newStop !== this.state.stop
            && newStop.isSameOrBefore(this.state.domain.max))
            || newStart.isSameOrAfter(this.state.domain.min)) {
            this.setState({
                start: newStart,
                stop: newStop,
                maxTimespan
            });
        }
    };

    onResizeRightCursor=(delta, mouse) =>  {
        let newStop = moment(this.xAxis.invert(this.xAxis(this.state.stop) + delta));
        let newStart = this.state.start;
        let maxTimespan = false;

        if(newStop.isSameOrBefore(newStart)){
            newStart = moment(this.xAxis.invert(this.xAxis(newStart) + delta));
        }

        if(this.props.biggestTimeSpan){
            const max = moment(newStart).add(this.props.biggestTimeSpan);
            if(max.isSameOrBefore(newStop)){
                newStop = max;
                maxTimespan = true;
            }
        }

        this.handleAutoSliding(mouse, this.onResizeRightCursor);

        if (newStop.isSameOrBefore(this.state.domain.max) &&
            newStart.isSameOrAfter(this.state.domain.min)){
            this.setState({
                start: newStart,
                stop: newStop,
                maxTimespan
            });
        }
    };

    onDragCursor = (delta, mouse) => {
        this.xAxis.clamp(false);
        const duration = this.state.stop.diff(this.state.start);
        let newStart = moment(this.xAxis.invert(this.xAxis(this.state.start) + delta));
        let newStop = moment(newStart).add(duration, 'MILLISECONDS');
        this.xAxis.clamp(true);

        if(this.props.maxDomain.min){
            newStart = moment.max([newStart, this.props.maxDomain.min]);
        }
        if(this.props.maxDomain.max){
            newStop = moment.min([newStop, this.props.maxDomain.max]);
        }

        this.handleAutoSliding(mouse, this.onDragCursor);

        this.setState({
            start: newStart,
            stop: newStop
        });
    };

    onStartDrawCursor = (pos) => {
        this.setState({
            start: moment(this.xAxis.invert(pos)),
            stop: moment(this.xAxis.invert(pos + 1)),
            maxTimespan: false
        });
    };

    onDrawCursor = (delta, mouse) => {
        let newStop = moment(this.xAxis.invert(this.xAxis(this.state.stop) + delta));
        let newStart = this.state.start;
        let maxTimespan = false;

        if(newStop.isSameOrBefore(newStart)){
            newStart = moment(this.xAxis.invert(this.xAxis(newStart) + delta));
        }

        if(this.props.biggestTimeSpan){
            const max = moment(newStart).add(this.props.biggestTimeSpan);
            if(max.isSameOrBefore(newStop)){
                newStop = max;
                maxTimespan = true;
            }
        }

        this.handleAutoSliding(mouse, this.onDrawCursor);

        if (newStop.isSameOrBefore(this.state.domain.max) &&
            newStart.isSameOrAfter(this.state.domain.min)){
            this.setState({
                start: newStart,
                stop: newStop,
                maxTimespan
            });
        }
    };

    onEndChangeCursor = () => {
        if(this.autoMove) {
            this.setAutoMove(false);
        }
        if(this.isAutoMoving){
            this.isAutoMoving = false;
            this.movedTimeLine();
        }
        this.props.onCustomRange(this.state.start, this.state.stop);
    };

    handleAutoSliding(mouse, action){
        //slide domain when dragging and mouse out of histo
        if(mouse){
            const posX = mouse[0];
            if(posX < 0){
                this.setAutoMove(true, action, posX);
            }
            else if(posX >= 0 && posX <= this.state.histoWidth){
                this.setAutoMove(false);
            }
            else if(posX > this.state.histoWidth ){
                this.setAutoMove(true, action, posX - this.state.histoWidth);
            }
        }
    }

    setAutoMove(active, action, delta){
        if(active){
            //prevent creating timer to often if mouse did not move so much
            if(!this.autoMove || (this.autoMove && Math.abs(this.lastAutoMovingDelta - delta) > 5)){
                clearInterval(this.autoMove);
                this.autoMove = setInterval(() => {
                    action(delta);
                    this.moveTimeLine(-delta);
                    this.isAutoMoving = true;
                    this.lastAutoMovingDelta = delta;
                },30);
            }
        }
        else if(!active){
            clearInterval(this.autoMove);
            this.autoMove = null;
        }
    }

    onWheel = (event) => {
        event.preventDefault();
        event.stopPropagation();

        if(!this.state.waitForLoad){
            if(event.deltaY < -50){
                if(this.state.maxZoom){
                    this.props.onShowMessage(this.labels.scrollMaxZoomMsg);
                }
                else{
                    const baseX = document.getElementById('timeLine').getBoundingClientRect().left;
                    const x = event.clientX - baseX - this.state.marginLeft;
                    this.zoomOnTarget(x);
                }
            }
            else if(event.deltaY > 50){
                this.zoomOut();
            }
        }
    };

    onGotoCursor = () => {
        const middle = moment(this.state.start).add(this.state.stop.diff(this.state.start)/2);
        this.xAxis.clamp(false);
        const currentX = this.state.histoWidth/2;
        const newX = this.xAxis(middle);

        const newDomain = this.moveTimeLineCore(currentX - newX);

        const domains = [...this.props.domains];
        //Replace current domain
        domains.splice(0,1,newDomain);

        //Check if other domains should shift
        const newDomains = this.shiftDomains(domains, newDomain);
        this.props.onUpdateDomains(newDomains);
    };

    onGoto = (d) => {
        const halfTimeAgg = this.state.stop.diff(this.state.start);
        const when = d || moment();
        const newStop = moment(when).add(halfTimeAgg);
        const newStart = moment(when).subtract(halfTimeAgg);

        this.props.onCustomRange(newStart, newStop);
    };

    zoomOnTarget = (x) => {
        const [minOrigin, maxOrigin] = this.xAxis.domain();
        const target = moment(this.xAxis.invert(x));

        const min = moment(target).subtract(0.25 * (target.diff(moment(minOrigin))));
        const max = moment(target).add(0.25 * (moment(maxOrigin).diff(target)));

        this.zoomOnDomain({
            min,
            max
        });
    };

    zoomOnDomain = (domain) => {
        if(!this.state.waitForLoad) {
            if(!domain.min.isSame(this.state.domain.min) || !domain.max.isSame(this.state.domain.max)){
                //Maximum zoom?
                const duration = domain.max.diff(domain.min);
                const width = this.state.histoWidth;
                let maxZoom = false;

                //We stop/recap zoom in when 15px = 1min
                const minRes = this.props.smallestResolution.asMilliseconds();
                if(duration < (width*minRes/15)) {
                    domain.max = moment(domain.min).add(width*minRes/15, 'ms');
                    this.props.onShowMessage(this.labels.zoomSelectionResolutionExtended);
                    maxZoom = true;
                }

                const domains = [...this.props.domains];
                domains.unshift(domain);

                this.setState({
                    maxZoom,
                    domain
                });
                this.props.onUpdateDomains(domains);
            }
            else{
                this.props.onShowMessage(this.labels.zoomInWithoutChangingSelectionMsg);
            }
        }
    };

    zoomIn = () => {
        if(!this.state.maxZoom){
            this.zoomOnDomain({
              min: this.state.start,
              max: this.state.stop
            })
        }
        else{
            this.props.onShowMessage(this.labels.doubleClickMaxZoomMsg);
        }
    };

    zoomOut = () => {
        if(this.props.domains.length > 1 && !this.state.waitForLoad){
            const domains = [...this.props.domains];
            domains.shift();

            this.setState({
                maxZoom: false,
                domain: domains[0]
            });
            this.props.onUpdateDomains(domains);
        }
    };

    moveTimeLineCore = (delta) => {
        this.xAxis.clamp(false);

        let min, max;
        if(this.props.domains.length === 1){ //at max zoom level
            if(delta < 0){
                min = this.state.domain.min;
                max = moment(this.xAxis.invert(this.state.histoWidth - delta));
            }
            else{
                min = moment(this.xAxis.invert(-delta));
                max = this.state.domain.max;
            }

            if(this.props.biggestVisibleDomain && moment(min).add(this.props.biggestVisibleDomain).isBefore(max)){
                min = moment(this.xAxis.invert(-delta));
                max = moment(min).add(this.props.biggestVisibleDomain);
            }
        }
        else{
            min = moment(this.xAxis.invert(-delta));
            max = moment(this.xAxis.invert(this.state.histoWidth - delta));
        }

        if(this.props.maxDomain.min){
            min = moment.max([min, this.props.maxDomain.min]);
            if(min.isSame(this.props.maxDomain.min)){
                delta = 0;
            }
        }
        if(this.props.maxDomain.max){
            max = moment.min([max, this.props.maxDomain.max]);
            if(max.isSame(this.props.maxDomain.max)){
                delta = 0;
            }
        }

        return {min, max};
    };

    moveTimeLine = (delta) => {
        const {min, max} = this.moveTimeLineCore(delta);

        this.xAxis.domain([min, max]);
        this.xAxis.clamp(true);

        this.ticks = this.xAxis.ticks(_floor(this.state.histoWidth / spaceBetweenTicks));

        this.movedSinceLastFetched += delta;
        if(Math.abs(this.movedSinceLastFetched) > 75 && this.props.fetchWhileSliding){
            this.movedSinceLastFetched = 0;
            this.getItems(this.props, {min, max});
        }

        this.setState({
            domain: {
                min,
                max,
            }
        });
    };

    movedTimeLine = () => {
        this.movedSinceLastFetched = 0;

        const domains = [...this.props.domains];
        //Replace current domain
        domains.splice(0,1,this.state.domain);

        //Check if other domains should shift
        const newDomains = this.shiftDomains(domains, this.state.domain);
        this.props.onUpdateDomains(newDomains);
    };

    shiftTimeLine = (delta) => {
        const incr = delta/Math.abs(delta)*8;
        let i = 0;
        function step(){
            this.moveTimeLine(incr);
            i += 8;
            if(i < Math.abs(delta)){
                setTimeout(step.bind(this), 10);
            }
            else{
                this.movedTimeLine();
            }
        }
        step.bind(this)();
    };

    static propTypes = {
        timeSpan: PropTypes.shape({
            start: PropTypes.instanceOf(moment).isRequired,
            stop: PropTypes.instanceOf(moment).isRequired
        }).isRequired,
        domains: PropTypes.arrayOf(PropTypes.shape({
            min: PropTypes.instanceOf(moment).isRequired,
            max: PropTypes.instanceOf(moment).isRequired
        })).isRequired,
        maxDomain: PropTypes.shape({
            min: PropTypes.instanceOf(moment),
            max: PropTypes.instanceOf(moment)
        }),
        biggestVisibleDomain: PropTypes.object,
        smallestResolution: PropTypes.object.isRequired,
        biggestTimeSpan: PropTypes.object,
        histo: PropTypes.shape({
            items: PropTypes.arrayOf(PropTypes.shape({
                time: PropTypes.instanceOf(moment).isRequired,
                metrics: PropTypes.arrayOf(PropTypes.number).isRequired,
                total: PropTypes.number.isRequired,
            })),
            intervalMs: PropTypes.number
        }).isRequired,
        metricsDefinition: PropTypes.shape({
            count: PropTypes.number.isRequired,
            legends: PropTypes.arrayOf(PropTypes.string).isRequired,
            colors: PropTypes.arrayOf(PropTypes.shape({
                fill: PropTypes.string.isRequired,
                stroke: PropTypes.string.isRequired,
                text: PropTypes.string.isRequired,
            })).isRequired
        }).isRequired,
        style: PropTypes.object,
        width: PropTypes.number,
        height: PropTypes.number,
        labels: PropTypes.shape({
            forwardButtonTips: PropTypes.shape({
                extendForward: PropTypes.string,
                slideForward: PropTypes.string,
            }),
            backwardButtonTips: PropTypes.shape({
                extendBackward: PropTypes.string,
                slideBackward: PropTypes.string,
            }),
            resetButtonTip: PropTypes.string,
            gotoNowButtonTip: PropTypes.string,
            doubleClickMaxZoomMsg: PropTypes.string,
            zoomInWithoutChangingSelectionMsg: PropTypes.string,
            zoomSelectionResolutionExtended: PropTypes.string,
            maxSelectionMsg: PropTypes.string,
        }),
        tools: PropTypes.shape({
            slideForward: PropTypes.bool,
            slideBackward: PropTypes.bool,
            resetTimeline: PropTypes.bool,
            gotoNow: PropTypes.bool,
            cursor: PropTypes.bool,
        }),
        fetchWhileSliding: PropTypes.bool,

        onLoadDefaultDomain: PropTypes.func.isRequired,
        onLoadHisto: PropTypes.func.isRequired,
        onCustomRange: PropTypes.func.isRequired,
        onShowMessage: PropTypes.func.isRequired,
        onUpdateDomains: PropTypes.func.isRequired,
        onResetTime: PropTypes.func.isRequired,
        onFormatTimeToolTips: PropTypes.func.isRequired,
        onFormatTimeLegend: PropTypes.func.isRequired,
        onFormatMetricLegend: PropTypes.func.isRequired,
    };

    static defaultProps = {
        tools: {
            slideForward: true,
            slideBackward: true,
            resetTimeline: true,
            gotoNow: true,
            cursor: true,
        }
    }
}
