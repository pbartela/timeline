import React, { Component } from 'react';
import {select} from 'd3-selection';

import TimeLine from './TimeLine';

const defaultWidth = 900;
const defaultHeight = 100;

export default class TimeLineResizer extends Component{
    constructor(props){
        super(props);

        this.state = {
            width: defaultWidth,
            height: defaultHeight,
        };
    }

    componentDidMount() {
        const container = select('#timeLineResizer');
        this.setState({
            width: container ? container.property('clientWidth') : defaultWidth,
            height: container ? container.property('clientHeight') : defaultHeight
        });

        window.addEventListener('resize',this.onViewResize.bind(this));
    }

    onViewResize(){
        const container = select('#timeLineResizer');
        this.setState({
            width: container ? container.property('clientWidth') : defaultWidth,
            height: container ? container.property('clientHeight') : defaultHeight
        });
    }

    render() {
        return  (
            <div
                style={this.props.style}
            >
                <div
                    id="timeLineResizer"
                    style={{width: '100%', height: '100%'}}
                >
                    <TimeLine
                        width={this.state.width}
                        height={this.state.height}
                        {...this.props}
                    />
                </div>
            </div>
        )
    }
}