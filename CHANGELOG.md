## 1.2.5
Visual improvements
- Move zoom icon further to the right
- Move left cursor icon when cursor is before view further to the left
- Removed white background (specific for Spider)

## 1.2.4
- Bug fix to take into account smallestResolution as minimum interval when calling aggregation API (was 1 min)
- Removed concatenation of smallestResolution.humanize() in labels.zoomSelectionResolutionExtended.
  * Was not working well.
  * New default: "You reached maximum zoom level"

## 1.2.3
- Many fixes after inclusion back to Spider
  - Load items on mount if domain is defined
  - Change reference for mouse events so that TimeLine can be included without TimeLineResizer
  - Visual fixes
  - Better handling of changing width when no items are loaded
  - Better handling of tools props
  - ...

## 1.2.2
- Fix: reattach event handlers after moving from cursor icon to cursor
- Fix: better handling on move to cursor => center domain on it (if possible)

## 1.2
- Add action on cursor icon to goto cursor
- Add tooltip for cursor icon
- Add tooltips for zoom in/out icons
- Remove componentWillReceiveProps (deprecated)

## 1.1
- Improve legend positioning
- Upgrade technical stack (React, D3, moment)
- Upgrade demo server stack (Koa, Webpack, Babel...)
- Improve deps for size + example of webpack optim (demo)
- maxDomain.min|max are both optional
- When on max zoom level with scrolling, display message and avoid jump (changing domain)
- Adapt left margin to legend 
- Move tools more to the right
- New prop to set what tools are active
- New prop to set max selectable duration.
    * Cursor and mouse pointer changes when max is reached.
    * A new message is displayed.
- Added showMessage toasts in demo
- Added autosliding when dragging with cursor outside chart area
- Added possibility to fetch data while sliding domain
- When selection cursor is outside view, show a small icon rather than the cursor 

## 1.0.5
- Fix index.js by removing jsx extension in require

## 1.0.4
- Fix package.json main point

## 1.0.1, 1.0.2, 1.0.3
- Fixes to limit module size on npm

## 1.0
- Transpile to ES5
- Add webpack config for demo
- Migrated to React 16
- Published to NPM
- Ability to limit max duration

## 0.1
- Extracted from Spider-GUI
- Removed dependency on Material UI (removed menu)
- Removed dependency on co
- Build sample without redux & saga
- Cleanup API: simplification & refactor
- Improved code quality & speed
- Added auto resize
- Added goto now feature
- Moved structure to NPM module with test app
- Flexible smallestDuration limit
- Better proptypes
- Label extraction for i18n
- Documentation (toc: markdown-toc --no-firsth1 README.md)