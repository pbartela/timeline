const path = require('path');
const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = (env, argv) => ({
    entry: [
        './src/index.jsx'
    ],
    output: {
        path: path.join(__dirname, 'bundle'),
        filename: '[name].js',
        publicPath: './static/'
    },
    mode: argv.mode === 'production' ? 'production' : 'development',
    devtool: argv.mode === 'production' ? undefined : 'eval-source-map',
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                include: [path.join(__dirname, 'src'),path.join(__dirname, 'timeline')],
                use: {
                    loader: "babel-loader",
                    options:{
                        presets: [
                            "@babel/preset-react"
                        ],
                        plugins: [
                            "@babel/plugin-syntax-dynamic-import",
                            "@babel/plugin-proposal-export-default-from",
                            "@babel/plugin-proposal-class-properties"
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            }
        ]
    },
    optimization: {
        splitChunks: {
            chunks: "all"
        }
    },
    plugins: [
        // Ignore all locale files of moment.js - 175K - https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new CompressionPlugin()
    ]
});