const path = require('path');
const koa = require('koa');
const router = require('koa-router')();
const serve = require('koa-static');
const fs = require('fs');

const applicationName = 'Timeline demo';
const isDevMode = process.argv[2] && process.argv[2] === '--dev';

const app = async () => {

    const app = new koa();
    app.name = applicationName;

    !isDevMode && app.use(require('koa-response-time')());

    if(isDevMode){
        app.use(require('@koa/cors')());

        const koaWebpack = require('koa-webpack');
        const webpackConfig = require('./webpack.config')({},{mode: 'development'});

        const middleware = await koaWebpack({
            config: webpackConfig,
            hotClient: {
                port: 5001,
                host: '0.0.0.0'
            },
            devMiddleware:{
                publicPath: '/static'
            }
        });
        app.use(middleware);
    }
    else {
        app.use(require('koa-compress')());
        app.use(require('koa-conditional-get')());
        app.use(require('koa-etag')());
        app.use(require('koa-mount')('/static',serve('bundle')));
    }

    app.use(serve('public'));

    router.get('/', async (ctx, next) => {
        await next();
        ctx.response.type = 'html';
        ctx.response.body = fs.createReadStream(path.join(__dirname, 'index.html'));
    });

    app.use(router.routes());

    const server = await new Promise(function (resolve, reject) {
        const server = app.listen(5000, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(server); //returns the server
            }
        });
    });

    console.log(`${applicationName} listening on port ${server.address().port}!`);

    process.on('SIGTERM', () => {
        server.close(() => {
            setTimeout(() => {
                console.log(`${applicationName} exited!`);
                process.exit(0);
            }, 1000)
        });
    });
};

app().catch(err => {
    console.log(`Couldn't launch application ${applicationName}`);
    process.exit(1);
});

