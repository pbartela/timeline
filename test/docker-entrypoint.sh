#!/bin/sh
set -e

if [ -e /parentHostName ]; then
    export PARENT_HOSTNAME=$(cat /parentHostName)
fi

if [ "$1" = "--" ]; then
    shift
    exec "$@"
else
    exec node "$@"
fi